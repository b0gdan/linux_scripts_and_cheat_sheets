dir_for_setup="/sites/bcgprometey.ru/"
find $dir_for_setup -type d -exec chmod -c -R 755 {} \;
find $dir_for_setup -type f -exec chmod -c -R 644 {} \;
find $dir_for_setup/wp-content/ -type d -exec chmod -c -R 775 {} \;
find $dir_for_setup/wp-content/ -type f -exec chmod -c -R 664 {} \;
chown nonroot:www-data -c -R $dir_for_setup
chown www-data:www-data -c -R $dir_for_setup7