# Linux bash cheet sheet/Набор полезных комманд при работе с Linux

### Operations with files/Операциии с файлами
Сменить директорию:
`cd /etc/ssh/ ### Change dir`
List files in that dir / Содержимое каталога
'ls'
### Copy all files from one folder to another
`cp -a /source/. /dest/`
### How delete a folder
`rm -r -f /home/bogdan/public_html/bogdan_cloud`
### Make file executable
`chmod a+x hello.py`
### How copy a file or a folder
`sudo cp /<from> /<to>`
### Remove all files from folder
`rm /<some_dir>`
### Get current dir
`pwd`
### Shows all files and folders in directory
`find <dir>`
### Create several files in directory
`touch /path/to/dir/{file1,file2,file3}`
### Create symbolic link
`ln -s /one/dir/or/file /path/to/symbolic/link`
### Creating new folder with specified path
`mkdir <new_directory>`
### Open home directory in default file manager
`gvfs-open /home/$USER`
`xdg-open /home/$USER`
### How extract a archives
```bash
tar --extract --verbose --gzip --file <archive_name>.tar.gz
tar --extract --verbose --bzip2 --file <archive_name>.tar.bz2 
tar --create --verbose --bzip2 --file archive.tar.bz2 file_or_directory.sql
tar --create --verbose --gzip --file archive.tar.gz file_or_directory.sql
	# x - extract
	# v - verbose output (lists all files as they are extracted)
	# j - deal with bzipped file
	# f - read from a file, rather than a tape device
```
### How mount webdav folder as drive. davfs need to been installed
`mount -t davfs https://cloud.bogdan.co/remote.php/webdav/ /media/owncloud/`
### How unmount
`umount /media/owncloud`
### Copy files between servers using SSH
`scp -P 2022 /<source_dir_or_file> nonroot@192.168.2.33:/<target_dir_or_file>`
### Upload: local -> remote
`scp local_file user@remote_host:remote_file`
### Download: remote -> local
`scp user@remote_host:remote_file local_file`
 ### Shows which files are only in dir1 and those only in dir2 and also the changes of the files present in both directories if any
`diff -r dir1 dir2`
### Shows which files are only in dir1
`diff -r dir1 dir2 | grep dir1`
### Recursively compare any subdirectories found and report only when files differ 
`diff --brief --recursive Cloud/Documents/ Cloud_old/Documents`
`crontab -e`
`df -h` # Shows in readable way information about partition sises
`du <folder> -h -s` # Shows in readable way folder size
### обычно все программы можно найти здесь 
`cd /usr/share/applications  # и те которые есть в меню, и те которых нет`	
### просмотр директорий самой программы 
`whereis <имя программы>`
`which <имя программы>`
### Как смонтировать .iso файл? 
### Mount the ISO in the target directory: 
`mount -o loop path/to/iso/file/YOUR_ISO_FILE.ISO /media/iso `
### Unmount the ISO: 
`umount /media/iso`
### Find all dublicates
`find -not -empty -type f -printf "%s\n" | sort -rn | uniq -d | xargs -I{} -n1 find -type f -size {}c -print0 | xargs -0 md5sum | sort | uniq -w32 --all-repeated=separate`
### Make a file Immutable/Write protected even for root running processes
The attribute that we will modify is i as in immutable.

`chattr +i foo`

```
# ls -l foo
-rwxrwxrwx 1 user user 4 Jun 9 22:30 foo
# echo "foo" >> foo
# chattr +i foo
# echo "foo" >> foo
-su: foo: Permission denied
# rm foo
rm: cannot remove `foo': Operation not permitted
```
Removing immutable attribute from a file/directory

`chattr -i foo`

`lsattr` command can be used to verify what attributes are set on a file/directory:
```
$ lsattr foo
----i--------e-- foo
```



## Settuping permissons for sites
`chgrp www-data /sites/ # Make sure the group is www-data on '/var/www'.`
### Make it writable
`sudo chmod 775 /sites/`
`chown -R nonroot:www-data /sites/`
### set group id for subfolders
`chmod g+s /sites/`
### Add user to sudo group
`adduser existing_user sudo`
### add your username to the group
`useradd -G www-data [USERNAME] ### OR`
`usermod -a -G www-data [USERNAME]`
### give yourself ownership 
`chown [USERNAME] /sites/`
### Setting permissions on folder, but not on content
`find /<some_dir> -type d -exec chmod 755 {} \;`
### Setting permissions on folder,and on content
```bash
chmod -R 755 /etc/openvpn
chmod -R 0755 /cloud
	# -R parameter makes all changes recursively
	# -c parameter log to terminal only changes
```
### Let Apache/Nginx be owner
`chown www-data:www-data  -R *`
### Change directory permissions rwxr-xr-x
`find . -type d -exec chmod 755 {} \;`
### Change file permissions rw-r--r--
`find . -type f -exec chmod 644 {} \;`
### Let your useraccount be owner
`chown <username>:<username>  -R * `
### Let apache be owner of wp-content
`chown www-data:www-data wp-content`

## How mount disk with ext4 file system
```bash
fdisk -l # Look at all disks in computer
mkdir /mnt/mountpoint  # Pick the name for "mountpoint" and use it throughout
chown -R yourusername: /mnt/mountpoint  # Example: sudo chown -R john: /mnt/mountpoint
blkid | grep "sdb1"  # Find the UUID of your new drive, assuming it's sdb1
cp /etc/fstab /etc/fstab.bak # Backup fstab
nano /etc/fstab # Open for editing
UUID=XXXXX-XXXXX-XXX /mnt/mountpoint ext4  defaults 0 2 # Add the entry to fstab using nano
mount /dev/sdb1 # Save and run the mount command
df -h # Check results
```

## Working with exfat in linux
```bash
sudo add-apt-repository universe
sudo apt update
sudo apt install exfat-fuse exfat-utils
```



## WORKING WITH LOGS
```bash
last -n 100 -f /var/log/btmp # Records only failed login attempts
last -n 100 -f /var/log/utmp # will give you complete picture of users logins at which terminals, logouts, system events and current status of the system, system boot time (used by uptime) etc
last -n 100 -f /var/log/wtmp # gives historical data of utmp
tail /var/log/fail2ban.log
cat /dev/null > /var/log/btmp # Очистка лога
```


## EDIT FILES IN TERMINAL
```bash
cat <path_2_file_name> ### Shows text file content in terminal
vi sshd_config ### Edit in vim or in emacs http://help.ubuntu.ru/wiki/vim
Esc # to quit edit mode and fallback to command mode ;
:wq # start with a colon and then press w and q to write and quit ;
:q! ### Exit without saving
Enter # then press to valid.
file --mime file.txt ### Show file encoding
iconv -f us-ascii -t utf-8 index.py > index.py
```
### Get first N lines from file to another
`head -n N logfile.csv > small_log.csv`

### remove first line of file
`sed -i '1d' logfile.csv`

### Take out lines from file according regexp
`awk '/.*1cv8c_.__IO.*/' transposed_logfile.csv > temp.csv`
### Transpose csv file
`apt install csvtool`
`csvtool transpose cutted_logfile.csv > transposed_cutted_logfile.csv`

## Selinux
Disable selinux:

`setenforce $1`


## OPERATIONS WITH PACKAGES (apt package manager)
```bash
apt list --upgradable # Packages for upgrade
apt-get update && apt-get dist-upgrade ### update all software
apt-get update && apt-get -y upgrade && apt-get -y dist-upgrade
apt-get update ### Updating all repositories
apt-get purge package ### If you want to reinstall config files as well you could do
apt-get install package ### If you want to reinstall config files as well you could do
apt-get install --reinstall package ### Reinstall packages with command prompt
apt-get --purge remove <package_name> ### Total uninstall of package
dpkg -i <package_name.deb> ### Installing .deb package 
dpkg --get-selections | grep -v deinstall ### List all packages installed
dpkg -l ### Another way of listing packages
export LC_ALL=C ### How to make english output in bash
``` 
### Show old versions in repository
`apt-cache show chromium-codecs-ffmpeg`
### Instal specific version
`apt install chromium-codecs-ffmpeg=49.0.2623.108-0ubuntu1.1233`
### Set up autoupdates
```bash
apt-get install unattended-upgrades
dpkg-reconfigure unattended-upgrades
nano /etc/apt/apt.conf.d/50unattended-upgrades
apt-mark showmanual # !!!!! all packages in the system
dpkg --list
```

## WORKING WITH NGINX
```bash
apt-get update
apt-get install nginx
ufw allow 'Nginx HTTP' ### Setup berfore ssl
ufw allow 'Nginx HTTPS' ### Setup berfore ssl
systemctl status nginx # Check nginx status
systemctl stop nginx
systemctl start nginx
systemctl restart nginx
systemctl reload nginx
systemctl disable nginx ### By default, Nginx is configured to start automatically when the server boots. If this is not what you want, you can disable this behavior
systemctl enable nginx ### Re-enable the service to start up at boot
cd /var/www/html ### Default folder for nginx
nano /etc/nginx/nginx.conf ### The main Nginx configuration file. This can be modified to make changes to the Nginx global configuraiton.
cd /etc/nginx/snippets ### This directory contains configuration fragments that can be included elsewhere in the Nginx configuration. Potentially repeatable configuration segments are good candidates for refactoring into snippets.
cat /var/log/nginx/access.log ### Every request to your web server
cat /var/log/nginx/error.log ### Any Nginx errors
chown -R www-data:www-data /var/www/example.com/public_html ### Grant ownership of the directory to the right user
chmod 755 /var/www ### Make sure that everyone is able to read our new files
apt-get -y install php7.0-fpm
apt-get install php7.0-mysql
/var/run/php/php7.0-fpm.sock ### unix-сокет PHP7. Необходимо указать его в настройкай сайта к сайту
apt-get -y install php7.0-mysql php7.0-curl php7.0-gd php7.0-intl php-pear php-imagick php7.0-imap php7.0-mcrypt php-memcache  php7.0-pspell php7.0-recode php7.0-sqlite3 php7.0-tidy php7.0-xmlrpc php7.0-xsl php7.0-mbstring php-gettext
service php7.0-fpm restart
### You need to concatenate the primary certificate file (your_domain_name.crt) and the intermediate certificate file (DigiCertCA.crt) into a single pem file
cat www.example.com.crt bundle.crt > www.example.com.chained.crt ### Объединение связки сертификатов в один. (Так происходит потому, что центр, выдавший сертификат, подписал его промежуточным сертификатом, которого нет в базе данных сертификатов общеизвестных доверенных центров сертификации, распространяемой вместе с браузером. В подобном случае центр сертификации предоставляет “связку” сертификатов).
```

## Information about processes and daemons
### Shows that the nginx master process is running as root
`ps aux -P | grep nginx`


## UFW
```bash
ufw enable # Enanble ufw to add nginx to it
ufw status verbose ### Check ufw status
ufw status ### Check ufw status
ufw app list # We can list the applications configurations that ufw knows how to work with
ufw status # Verify the change
ufw allow <port number>/tcp # How enable for example 2022 port instead default 22
ufw default deny incoming && ufw default allow outgoing ### Set the defaults used by UFW
ufw allow ssh
ufw allow 22 ### The same s command above
ufw reset
```

## WORKING WITH LAMP
```bash
sudo service apache2 reload
sudo service apache2 restart ### Killes current prosess and runs it again
sudo /etc/init.d/apache2 reload
apachectl configtest ### Test apache2 configuration
```

## PYTHON
`sudo python3 -m http.server 8000 ### Start simple server`

### https://www.digitalocean.com/community/tutorials/how-to-set-up-an-apache-mysql-and-python-lamp-server-without-frameworks-on-ubuntu-14-04
```bash
python --version ### Check current Python version.
sudo rm /usr/bin/python ### Removing the old 2.7 binary.
sudo ln -s /usr/bin/python3 /usr/bin/python ### Create a symbolic link to the Python 3 binary in its place.

sudo apt-get install python3-pip
python -V ### Python version
pip install virtualenv ### Installing a tool to create isolated Python environments.
virtualenv -p python3 <name of virtual environment> ### Make directory to place the new virtual environment
virtualenv <name of virtual environment> ### Make directory to place the new virtual environment
source <name of virtual environment>/bin/activate ### Activate script
deactivate ### Simple
```

## NETWORKING & IP ADRESSES
```bash
nmcli c ### List all saved connections
ifconfig ### Shows information about current IP
ip a ### Show current interfaces
ip -4 a ### Only want to see IPv4 information
ip -4 a show wlp3s0 ### Info abaout interface
nmcli c down <SavedConn> ### disconnect
nmcli c up <SavedConn> ### connect
nmcli radio wifi ### Get Wifi status
nmcli radio wifi <on|off> ### Turn wifi on or off
nmcli device wifi list ### List available access points(AP) to connect to
nmcli device wifi rescan ### Refresh previous list
nmcli device wifi connect <SSID|BSSID> ### Create a new connection to an open AP
nmcli device wifi connect <SSID|BSSID> password <password> ### Create a new connection to a password protected AP
arp-scan --interface=enp0s25 --localnet ### Scan devices in local network
nc -zv 91.122.34.89 7443 ### Test port
```

## WORKING WITH SSH
```bash
adduser # Add new unix user
apt-get install ssh # Installation
service ssh start # Start
systemctl enable ssh #  For automatically SSh start on boot
ufw default deny incoming && sudo ufw default allow outgoing ### Set the defaults used by UFW
service ssh restart
Subsystem sftp internal-sftp ### This adds an ability to use sft from the root folder, it should be added to /etc/ssh/sshd_config
useradd -g sftpusers -d /home/bogdan/public_html -m -s /bin/false bogdan_sftp
sudo nano /etc/rc.local ### How add ssh to outostart !__STEP 1__!
service ssh restart ### Add this line to rc.local - How add ssh to outostart !__STEP 2__!
```
### Создать proxy-туннель на localhost с портом 5655, указав при этом кастомный порт SSH демона:
`ssh -D 5655 -C bogdan@myip.bogdan.co -p 2022 -f -N`
### То же самое, но уже не на localhost, а на внешнем интерфейме для доступа компьютеров из сети
`ssh -D 0.0.0.0:5655 -C bogdan@myip.bogdan.co -p 2022 -f -N`
### Get server public key fingerprint:
```
ssh-keygen -l -E sha256 -f /etc/ssh/ssh_host_ed25519_key.pub
ssh-keygen -l -E md5 -f /etc/ssh/ssh_host_ed25519_key.pub
```
### Create user for SSH proxy

1. As `root`, create a new file in `/bin` directory, let say `fakesh`: `nano /bin/fakesh`
2. Add the following code inside `/bin/fakesh` file:
```
#!/bin/bash
echo -n "$ "
while read cmd ; do 
    if [ "$cmd" = "exit" ]; then break; fi
    if [ "$cmd" != "" ]; then echo "The only one available command is: exit"; fi
    echo -n "$ "
done
```
3. Save the file and make it executable for everyone: `chmod +x /bin/fakesh`
4. After these, just set `/bin/fakesh` as the default shell for your user. The user will be available to login his account via ssh, but he will not be able to execute any command except for `exit`.

## GET INFORMATION about system
```bash
screenfetch
mysql -u root -p -e 'SHOW VARIABLES LIKE "%version%";' ### Information about device from mysql in pretty form
sudo dmidecode -q ### General device name
cat /proc/version ### About OS
cat /proc/cpuinfo ### About CPU
sudo update-pciids && sudo lspci -v ### Grab the current version of the pci.ids file from the Internet && Get the vendor and model of devices
lspci -k | grep -EA2 'VGA|3D' ### What graphic drivers are installed
lscpu ### About CPU
lspci -nnk | grep VGA -A1 ### About GPU
lspci -v | grep -A7 -i "audio" ### Audio
lspci -nnk | grep net -A2 ### Network
nvidia-smi -q -d temperature ### Get current nvidia graphics temperature
sensors ### Processor's temperature
sudo lspci -v
service --status-all ### Get all running services
locate <PATTERN> ### How to find files by name. Can be ased to locate some libraries or programs
nc -zv 91.122.34.89 7443 ### Check port availability
watch ps aux | grep '[p]rocess_name' # Find pricess in ps by its name. [] Are used for the first symbol to avoid displaying grep process
```

## Settings for programming enviroments
### Shows the current path
`echo $PATH` 
### Add enviroment to PATH
`export PATH=$PATH:/home/bogdan/Programs/activator-dist-1.3.10/bin/`
### Restore default PATH
`export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin`

### Java Home and path to Java
`export JAVA_HOME=/path/to/java/jre/or/jdk`
`export PATH=${PATH}:${JAVA_HOME}/bin`


## Encrypted file system
`mount -t ecryptfs /srv /srv`
`umount /srv`
<a href="http://help.ubuntu.ru/wiki/руководство_по_ubuntu_server/безопасность/ecryptfs">Руководство по ecryptfs</a>


## Samba
### Показывает всех unix пользователей
`compgen -u`
### Показывает всех unix группы
`compgen -g`
### Все пользователи samba
`pdbedit -L -v`
### Добавить unix пользователя 
`adduser temp_user`
### Добавить пользователя samba
`smbpasswd -a temp_user`
### Config
```
[temp_user]
path = /srv/smb/temp_user
valid users = temp_user
read only = no
```
### Протестировать параметры
`testparm`
### Перезапустить samba-демон
`service smbd restart`


## Davfs
`mount -t davfs https://cloud.balticland.ru/remote.php/webdav/ /srv/smb/temp_user/`

## GRUB (Комманды в консоли GRUB)
### Выводит все доступные диски. необходимо с помощью этой команды найти нужный диск и загрузиться с него: /boot/
`ls`
`set root=(hd0,msdos2)`
`linux /boot/vmlinuz-4.4.0-83-generic root=/dev/md126p2`
`initrd /boot/initrd.img-4.4.0-83-generic`
`boot`
### потом все эти команды добавить в конфиг grub'а
`nano /boot/grub/grub.cfg`

## Macbook
### High-DPI in Sublime on linux:
`"dpi_scale": 2.0`
`@reboot echo 2 > /sys/module/hid_apple/parameters/fnmode`


## Change Linux kernel configs
```shell
sudo nano /etc/default/grub
```

Settings for switching to nouveau: https://rpmfusion.org/Howto/NVIDIA
```config
GRUB_CMDLINE_LINUX="rd.luks.uuid=luks-7dc02619-ae88-43c7-8bbb-2789a288523e rhgb quiet"
```

Cite from the website:
```text
Switching between nouveau/nvidia

With recent drivers as packaged with RPM Fusion, it is possible to switch easily between nouveau and nvidia while keeping the nvidia driver installed. When you are about to select the kernel at the grub menu step. You can edit the kernel entry, find the linux boot command line and manually remove the following options "rd.driver.blacklist=nouveau modprobe.blacklist=nouveau nvidia-drm.modeset=1". This will allow you to boot using the nouveau driver instead of the nvidia binary driver. At this time, there is no way to make the switch at runtime. 
```

```
sudo grub2-mkconfig -o /boot/grub2/grub.cfg
sudo reboot
```

## Arch linux
### установить пакет
`pacman -S package`
### удалить пакет 
`pacman -R package`
### обновить пакет
`pacman -Su package`
### список файлов пакета
`pacman -Q1 package`
### установить принадлежность файла какому-либо пакету
`pacman -Qo filename`
### обратные зависимости от пакета
`whoneeds package`
### список пакетов-сирот, без обратных зависимостей
`pacman -Qdt`
### простой полнотекстовый поиск
`pacman -Ss package`
### поиск с регулярными выражениями
`pacman -Ss '^vim-'`
### поиск среди установленных пакетов
`pacman -Qs package`

### Обновить всю систему
`pacman -Syu`
### Upgrading_packages
<a href="https://wiki.archlinux.org/index.php/Pacman">Pacman docs</a>
<a href="https://wiki.archlinux.org/index.php/silent_boot">https://wiki.archlinux.org/index.php/silent_boot</a> 


## Прочие полезности и прикольные вещи:
### Weather forecast
`curl wttr.in/Saint-Petersburg,Russia?1`
### Restart wi-fi adapter
`service network-manager restart ### Restart WLAN (Wi-Fi)`
### Shutdown PC/Server 
`shutdown -h now`
`poweroff`


## Working with CentOS
### Update whole system 
`yum update`
### Nano as default crontab editor
`export VISUAL=nano; crontab -e`
### How to Setup network on centos 7
`nmcli d` Select appropriate interface
`nmtui`

After setup enter `systemctl restart network`

### List all public interfaces:
`firewall-cmd --get-active-zones`
### Open firewall port on CentOS 7
`firewall-cmd --zone=public --add-port=2888/tcp --permanent`

`firewall-cmd --permanent --zone=public --add-service=http `

`firewall-cmd --permanent --zone=public --add-service=postgresql`
### Reload the firewall for changes to take effect
`firewall-cmd --reload`
### Make an Nginx user to be an owner of the files in acme dirs
`chown -R -c 997:993 /home/bogdan/.acme.sh/`
### Change permissions on acme files
`find /home/bogdan/.acme.sh/ -type d -exec chmod 700 {} \;`
### Change permissions on acme dirs
find /home/bogdan/.acme.sh/ -type f -exec chmod 600 {} \;
### Install rpm package from the internet
`rpm -Uhv http://nginx.org/packages/centos/7/x86_64/RPMS/nginx-1.10.0-1.el7.ngx.x86_64.rpm`

