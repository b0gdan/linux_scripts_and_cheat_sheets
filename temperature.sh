#!/bin/bash
IFS=$'\n'       # make newlines the only separator
for cpu_temp in $(cat /sys/class/thermal/thermal_zone*/temp)    
do
    echo $(($cpu_temp/1000))°C
done
