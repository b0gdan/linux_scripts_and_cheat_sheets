1. На странице производителя скачайте последнюю прошивку для своего устройства:

`https://www.boox.com/downloads/#nova-series`

2. К сожалению, прошивка зашифрована, поэтому нужно достать сначала код для декодирования. Подключаемся через adb к телефону и следующей командой вытягиваем системное приложения для обновления ПО.

`adb pull /system/app/OnyxOtaService/OnyxOtaService.apk`

3. Как только архив с приложением появился в нашей текущей папке, разархивируем его с помощью следующей команды:

`java -jar ~/Downloads/apktool_2.4.1.jar d OnyxOtaService.apk`

apktool можно скачать тут: (https://github.com/iBotPeaches/Apktool). Для его запуска нужна Java минимально 8 версии.

4. После распаковки архива с приложением в ./OnyxOtaService/res/values/strings.xml Вы найдёте что-то вроде 

```xml
<string name="settings">lxXh4Vv6aqYecCAFc/hsn4mnXNbI6H4S3bZFW5Jh8NHj</string>
<string name="upgrade">lBabky+FbaOtZ7luDK+7BlApiYcGEi8PndwIc5WaemXQ</string>
<string name="local">iDDDo3jsN4hhLA3tQhaIkM4XLcxZT4czBMM7ExnK</string>
```

Эту всю прелесть вставляем в скаченный отсюда скрипт: https://github.com/Hagb/decryptBooxUpdateUpx/blob/master/DeBooxUpx.py

Получится что-то вроде (в моём случае):

```json
'Nova2':{
    "MODEL": "Nova2",
    "STRING_7F00500": "lxXh4Vv6aqYecCAFc/hsn4mnXNbI6H4S3bZFW5Jh8NHj",
    "STRING_7F00501": "lBabky+FbaOtZ7luDK+7BlApiYcGEi8PndwIc5WaemXQ",
    "STRING_7F00502": "iDDDo3jsN4hhLA3tQhaIkM4XLcxZT4czBMM7ExnK"
}
```


5. Добавляем в конец скрипта следующие строки:
```python
from DeBooxUpx import DeBooxUpx, boox_strings
model = 'Nova2'
updateUpxPath = 'update.upx'
decryptedPath = 'update.zip'

decrypter = DeBooxUpx(**boox_strings[model])
print('When updating, the device decrypt update package into', decrypter.path)
decrypter.deUpx(updateUpxPath, decryptedPath)
```



6. Не забываем установить сам питон в систему и зависимости для скрипта:
```shell
pip install pycryptodomex
pip install pycryptodome
```

7. После чего запускаем декодирование архива:
```shell
python DeBooxUpx.py
```

8. В текущей папке появится update.zip, распаковываем его и кидаем из него на телефон файл с ядром Linux: boot.img

9. Устанавливаем Magisk Manager и с помощью него патчим ядро. Копируем пропатченное ядро обратно на комп и перезагружаемся в fastboot:
```shell
adb install ~/Downloads/MagiskManager-v8.0.0.apk
adb push ~/Downloads/redfin-rd1a.200810.021.a1/image-redfin-rd1a.200810.021.a1/boot.img /sdcard/Download
adb pull /sdcard/Download/magisk_patched.img ~/Downloads
adb reboot bootloader # На этом моменте экран будет выглядеть так, как будто ничего не случилось из-за e-ink, но если вы запустите "fastboot devices", ваше устройство должно появиться. 
fastboot flash boot ~/Downloads/magisk_patched.img
fastboot reboot
```

10. Ура, у нас установлен рут!

P.S. Возможно понадобится разблокировка загрузчика:
```shell
fastboot oem unlock
```

А если эта команда не помогла, то:
```shell
fastboot flashing unlock
```
