## Manually give root privileges to the app (Magisk)

1. Copy the DB with the magisk's setting to the computer to open `/data/adb/magisk.d` in DBeaver.

2. Get necessary UID in the following file:

`/data/system/packages.list` or  `/data/system/packages.xml`

2. In the Table `policies` insert a row with settings in the following format:
```
|uid  |package_name                |policy|until|logging|notification|
|:----|:--------------------------:|:----:|:---:|:-----:|-----------:|
|10100|dev.ukanth.ufirewall        |     2|    0|      1|           1|
|10086|com.github.axet.filemanager |     2|    0|      1|           1|
```
3. Stop Magisk

4. Replace original .db file.


## Where in the file system are applications installed?

`/system/app`  or  `/system/priv-app` or `/product/app` or `/product/priv-app` 

[Where in the file system are applications installed](https://android.stackexchange.com/questions/3002/where-in-the-file-system-are-applications-installed)

## Uninstall bloatware without root:
```shell
adb shell
pm list packages -f # List all installed packages
pm uninstall --user 0 <package name> #1 To uninstall an app with its data
pm uninstall -k --user 0 <package name> # To uninstall an app but keep its data
pm uninstall <package name> # On pixel
```

## Remount to read-write system
mount -o rw,remount -t ext4 /dev/block/dm-3

## Reinstall Uninstalled Android Apps
```shell
cmd package install-existing <package name>
```
[More info](https://technastic.com/freeze-uninstall-system-apps-android/)
