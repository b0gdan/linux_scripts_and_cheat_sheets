server {
    server_name www.cloud.bogdan.co;
    return 301 $scheme://cloud.bogdan.co$request_uri;
}

server {
    listen *:80;
    server_name cloud.bogdan.co;
    proxy_set_header Host cloud.bogdan.co;
    location / {
        return 301 https://$host$request_uri;
    }
}

server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;
    server_name cloud.bogdan.co;
    proxy_set_header Host cloud.bogdan.co;

    set $account_ip 172.18.0.3;
    set $account_port 80;

    ssl_certificate /home/bogdan/.acme.sh/bogdan.co/fullchain.cer;
    ssl_certificate_key /home/bogdan/.acme.sh/bogdan.co/bogdan.co.key;
    ssl_session_timeout 1d;
    ssl_session_cache shared:MozSSL:10m;  # about 40000 sessions
    ssl_session_tickets off;

    # curl https://ssl-config.mozilla.org/ffdhe2048.txt > /path/to/dhparam.pem
    ssl_dhparam /etc/nginx/dhparam.pem;

    ssl_protocols TLSv1.2 TLSv1.3;
    ssl_ciphers ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384;
    ssl_prefer_server_ciphers off;

    # HSTS (ngx_http_headers_module is required) (63072000 seconds)
    add_header Strict-Transport-Security "max-age=63072000" always;

    # OCSP stapling
    # ssl_stapling on;
    # ssl_stapling_verify on;

    # verify chain of trust of OCSP response using Root CA and Intermediate certs
    # ssl_trusted_certificate /path/to/root_CA_cert_plus_intermediates;


    client_max_body_size 10G;
    fastcgi_buffers 64 4K;
    client_body_buffer_size 16k;

    gzip on;
    gzip_vary on;
    gzip_proxied any;
    gzip_comp_level 6;
    gzip_buffers 16 8k;
    gzip_http_version 1.1;
    gzip_types application/rss+xml application/vnd.geo+json application/vnd.ms-fontobject application/x-font-ttf application/x-web-app-manifest+json application/xhtml+xml font/opentype image/bmp image/svg+xml image/x-icon text/cache-manifest text/vcard text/vnd.rim.location.xloc text/vtt text/x-component text/x-cross-domain-policy application/atom+xml application/javascript application/ld+json application/manifest+json text/plain text/css application/json application/x-javascript text/xml application/xml application/xml+rss text/javascript;

    error_log /srv/cloud.bogdan.co/logs/error.log;

    location = /robots.txt {
        allow all;
        log_not_found off;
        access_log off;
    }

    location = /.well-known/carddav {
        return 301 $scheme://$account_ip:$account_port/remote.php/dav;
    }
    location = /.well-known/caldav {
        return 301 $scheme://$account_ip:$account_port/remote.php/dav;
    }


    location ~ ^\/(?:build|tests|config|lib|3rdparty|templates|data)\/ {
        deny all;
    }
    location ~ ^\/(?:\.|autotest|occ|issue|indie|db_|console) {
        deny all;
    }

    location / {
        proxy_read_timeout 180000;
        proxy_connect_timeout 180000;
        proxy_send_timeout 180000;
        send_timeout 180000;
        proxy_pass http://$account_ip:$account_port;
    }

}