# Configuring raspberry infrastructure

## Installing necessary software
apt install haveged nginx 


## Remote port forwarding with SSH

Suppose, you’re developing a web application that’s running on port 8000 of your local computer.
Other people can’t access it directly because you’re sitting behind a NAT network without a public IP.
Now, you would like to show a customer how the application looks like.


When you run this command: `ssh -R *:7000:127.0.0.1:8000 bogdan@nl.bogdan.co`, the SSH server binds to the 7000 port on nl.bogdan.co.
Any traffic that it receives on this port is sent to the SSH client on your local computer,
which in turn forwards it to port 8000 on 127.0.0.1. 


The following command for Raspberry PI 4:
```shell script
ssh \
-R localhost:2222:127.0.0.1:22 \
-R *:80:localhost:80 \
-R *:443:localhost:443 \
-R *:5432:localhost:5432 \
root@nl.bogdan.co
```
will
* forward SSH traffic from remote 2222 port to the standart 22 local port
* forward 443, 80 ports for nginx to the same local ports


The following command for Raspberry PI 3:
```shell script
ssh \
-R localhost:2220:127.0.0.1:22 \
-R *:5432:localhost:5432 \
nl.bogdan.co
```
will
* forward SSH traffic from remote 2220 port to the standart 22 local port
* forward Postgres traffic from remote 5432 port to the 5432 local port 

**AutoSSH RPI 4**
```shell script
autossh \
-M 0 \
-f \
-q \
-N -o "ServerAliveInterval 10" -o "ServerAliveCountMax 3" \
-R localhost:2222:127.0.0.1:22 \
-R *:80:localhost:80 \
-R *:443:localhost:443 \
-R *:5432:localhost:5432 \
nl.bogdan.co
```


**AutoSSH  RPI 3**
```shell script
autossh \
-M 0 \
-f \
-q \
-N -o "ServerAliveInterval 10" -o "ServerAliveCountMax 3" \
-R 0.0.0.0:2220:localhost:22 \
nl.bogdan.co
```

## Setting up user with limited rights for proxying and port forwarding

1. create user
```shell script
ssh_proxy_user:x:1000:1000:,,,:/home/ssh_proxy_user:/bin/fakesh
```
2. Add following fake shell
```shell script
#!/bin/bash
echo -n "$ "
while read cmd ; do 
    if [ "$cmd" = "exit" ]; then break; fi
    if [ "$cmd" != "" ]; then echo "The only one available command is: exit"; fi
    echo -n "$ "
done
```



## Nginx test config
```shell script
server {
        listen 8000 default_server;
        listen [::]:8000 default_server;

        server_name _;

        root  /home/bogdan/Programming/JavaScript/cv/project/dist/;
        index index.html;

        location / {
                try_files $uri $uri/ /index.html?uri=$uri;
        }

        location @rewrites {
                rewrite ^(.+)$ /index.html last;
        }
}
```



## Sources

[A Guide to SSH Port Forwarding/Tunnelling](https://www.booleanworld.com/guide-ssh-port-forwarding-tunnelling)

[SSH Port Forwarding Example - detailed](https://www.ssh.com/ssh/tunneling/example)


