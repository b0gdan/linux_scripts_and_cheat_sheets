# Autossh alternative via systemd and SSH

## Step 1. Create SSH config

```bash
nano /root/.ssh/config
```

```yaml
Host nl.bogdan.co
  HostName nl.bogdan.co
  User root
  IdentityFile /root/.ssh/nl.bogdan.co
  IdentitiesOnly yes
  RemoteForward localhost:2222 localhost:22
  RemoteForward nl.bogdan.co:80 localhost:80
  RemoteForward nl.bogdan.co:443 localhost:443
  RemoteForward nl.bogdan.co:5432 localhost:5432
  ServerAliveInterval 60
  ExitOnForwardFailure yes
```


## Step 2. Create systemd unit file
```bash
nano /etc/systemd/system/secure-tunnel@.service
```

```bash
[Unit]
Description=Setup a secure tunnel to %I
After=network-online.target ssh.service

[Service]
User=root

ExecStart=/usr/bin/ssh -F /root/.ssh/config -NT %i

# Restart every >2 seconds to avoid StartLimitInterval failure
RestartSec=5
Restart=always

[Install]
WantedBy=multi-user.target
```

## Step 3. Enable service for autostart on boot and start it for the fors time manually
```bash
systemctl enable secure-tunnel@nl.bogdan.co
systemctl start secure-tunnel@nl.bogdan.co.service
```

## Sources

[Setup a secure (SSH) tunnel as a systemd service](https://gist.github.com/drmalex07/c0f9304deea566842490)

[Systemd service for autossh](https://gist.github.com/thomasfr/9707568)