# Запись образа операционки на MicroSD карту

DietPi - minimalistic OS for raspberry

1. [Download](https://minibianpi.wordpress.com/download/) OS and verify it using both md5sum and sha256sum: 
2. Unmount microsd card. Example:
unmount /dev/sdd1
3. Write image to SD card:
dd if=/home/bogdan/Downloads/2016-03-12-jessie-minibian.img | pv | dd of=/dev/sdc
# Make image from SD card:
dd if=/dev/sdc of=./SDCardBackup.img

# Автостарт ssh
1. Активировать автоподключение по wi-fi:
When you are logged in and connected to the network, right-click the Network Manager icon. (It should be in the upper right of the screen.)
Click "Edit Connections..."
Find the connection you want to make available without login. Click it and click the "Edit" button.
Make sure the "Connect automatically" and "Available to all users" boxes are checked.
Now the connection will start up before anyone logs in and will be available to everyone on the system.
2. Активировать автозапуск SSH:
```shell script
service ssh enable
```
Or
```shell script
systemctl enable ssh
```



