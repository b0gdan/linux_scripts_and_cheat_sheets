# Add to firefox native kde file dialogs

```
apt install xdg-desktop-portal xdg-desktop-portal-kde
nano ~/.local/share/applications/firefox.desktop
```

```
[Desktop Entry]
Version=1.0
Name=Firefox Web Browser
Name[ru]=Веб-браузер Firefox
Comment=Browse the World Wide Web
Comment[ru]=Доступ в Интернет
GenericName=Web Browser
Keywords=Internet;WWW;Browser;Web;Explorer
Keywords[ru]=Internet;WWW;Browser;Web;Explorer;интернет;браузер;веб;файрфокс;огнелис
Exec=GTK_USE_PORTAL=1 /usr/lib/firefox/firefox %u
Terminal=false
X-MultipleArgs=false
Type=Application
Icon=firefox
Categories=GNOME;GTK;Network;WebBrowser;
MimeType=text/html;text/xml;application/xhtml+xml;application/xml;application/rss+xml;application/rdf+xml;image/gif;image/jpeg;image/png;x-scheme-handler/http;x-scheme-handler/https;x-scheme-handler/ftp;x-scheme-handler/chrome;video/webm;application/x-xpinstall;
StartupNotify=true
Actions=new-window;new-private-window;

[Desktop Action new-window]
Name=Open a New Window
Name[ru]=Новое окно
Exec=GTK_USE_PORTAL=1 /usr/lib/firefox/firefox %u

[Desktop Action new-private-window]
Name=Open a New Private Window
Name[ru]=Новое приватное окно
Exec=GTK_USE_PORTAL=1 /usr/lib/firefox/firefox -private-window
```

