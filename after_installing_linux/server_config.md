# Helpful tips after installing Ubuntu/Debian on the server

## CentOS

### How To Change Timezone on a CentOS 6 and 7

```bash
date
```

```bash
ls -l /etc/localtime
```

```bash
timedatectl | grep -i 'time zone'
```

```bash
timedatectl list-timezones | grep Berlin
```

```bash
timedatectl set-timezone Europe/Berlin
```


## Delete useless software
```
apt purge lxd lxd-client lxcfs
apt purge snapd
```

## Helpful software installation
* Исправить кодировку

`xdg-open "file://~/Cloud/Файлы/Документы Закладки/Администрирование/Linux/Установка русской локали в Debian или Ubuntu.pdf"`

```
apt install locales
locale-gen "en_US.UTF-8"
locale-gen ru_RU
locale-gen ru_RU.UTF-8
update-locale
```


* duplicates finder
```
apt install fdupes
fdupes -r # Recursive search for dublicates
```

* Добавление красивового логина в баш при инициализации ssh сессии
```
apt install screenfetch
echo -e "\n# Shows all the system information">> ~/.bashrc
echo -e "if [ -f /usr/bin/screenfetch ];\n\tthen screenfetch;\nfi" >> ~/.bashrc
```

* Midnight Commander

`apt install mc`

* Best process manager 
```
apt install htop #Terminal task manager
```

* Docker

`xdg-open https://www.docker.com/`

* Nginx
```
apt install nginx
ufw allow 'Nginx HTTP'
```

* MySQL server installation
```
apt install mysql-server
mysql_secure_installation
bind-address=0.0.0.0 # Сделать доступным MySQL сервер доступным удалённым
```