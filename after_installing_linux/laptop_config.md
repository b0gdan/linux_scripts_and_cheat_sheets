# Helpful tips after installing Ubuntu on the desktop



## Everything for Gnome

### Install "GNOME Shell Extensions"

### Install Flathub repository

### Install [Flatseal](https://flathub.org/apps/details/com.github.tchx84.Flatseal)

### Install [Progressive Web Apps for Firefox](https://github.com/filips123/PWAsForFirefox) to Firefox

### Manually add Wireguard. Check for the actuality!
```shell
nmcli connection import type wireguard file path/to/your/wg.conf
```


### Scale display at 150%

```shell
gsettings set org.gnome.mutter experimental-features "['scale-monitor-framebuffer']"
```

Disable this setting:

```shell
gsettings reset org.gnome.mutter experimental-features
```






# Legacy settings


## Helpful software installation
* Установить графику Intel
```
xdg-open https://01.org/linuxgraphics/downloads/firmware
xdg-open https://01.org/linuxgraphics/downloads/intel-graphics-update-tool-linux-os-v2.0.2
```

* Fix issue with power management
Problem:
```
Nov  9 11:20:38 debian kernel: [69300.887669] pcieport 0000:00:02.0: AER: Multiple Corrected error received: id=0010
Nov  9 11:20:38 debian kernel: [69300.887680] pcieport 0000:00:02.0: PCIe Bus Error: severity=Corrected, type=Data Link Layer, id=0010(Transmitter ID)
Nov  9 11:20:38 debian kernel: [69300.889913] pcieport 0000:00:02.0:   device [8086:6f04] error status/mask=00001100/00002000
Nov  9 11:20:38 debian kernel: [69300.892156] pcieport 0000:00:02.0:    [ 8] RELAY_NUM Rollover
Nov  9 11:20:38 debian kernel: [69300.894370] pcieport 0000:00:02.0:    [12] Replay Timer Timeout
```
Solution:
```bash
nano /etc/default/grub # Open config
GRUB_CMDLINE_LINUX_DEFAULT="quiet pcie_aspm=off" # Add kernel parameter
update-grub # Apply changes
```

* Установить Google Chrome

`apt install chromium`

* Установить менеджер удалённый рабочих столов

`apt install remmina`

* Установить менеджер паролей

`apt install keepassxc`

* Установить медиаплеер
```bash
add-apt-repository ppa:videolan/stable-daily
add-apt-repository ppa:videolan/master-daily
apt install vlc
```
* Webcam app

`apt install cheese`

* To mount webdav resources as drives

`apt install davfs`

* Program to pick a color

`apt install kcolorchooser`


* Установить torrent качалку
`apt install ktorrent`

* Установить драйвер видеокарты (проверить последнюю версию)
`apt install nvidia-*`

* Привести в порядок libreoffice в среде KDE
```bash
# Удаляем, если есть, старый офис
apt remove --purge libreoffice-*
# Добавляем репозиторий с новым
add-apt-repository ppa:libreoffice/ppa
# Устанавливаем новый офис
apt install --no-install-recommended libreoffice-writer libreoffice-calc libreoffice-impress libreoffice-kde5
# И выбрать тему Elementary, а потом установить локализацию и проверку орфографии:
apt install myspell-ru firefox-locale-ru aspell-ru
```

* Установить программу для разметки дисков

`apt install gparted`

* Grub Customizer
```
add-apt-repository ppa:danielrichter2007/grub-customizer
apt update
apt install grub-customizer
```

* Krita

`apt install krita`


* Установить это пакет, чтобы были полные настройки для тачпада

`apt install xf86-input-synaptics`


## Setting and configurations
* Включить wi-fi на asus k500ux

`xdg-open http://askubuntu.com/questions/754984/a-list-of-wireless-connections-doesnt-appear-in-asus-k501ux#comment1126868_754990`

* Вернуть на маке нормальную работу Fn клавиш

`echo *> /sys/module/hid_apple/parameters/fnmode`

* Нормальные emoji для linux
```bash
https://github.com/eosrei/emojione-color-font#install-on-ubuntu-linux
apt install software-properties-common
apt-add-repository ppa:eosrei/fonts
apt update
apt install fonts-emojione-svginot
```

* Исправить дефолтную locale в терминале:

Выбрать нужные locale:

`dpkg-reconfigure locales`

После этого они должны сгенерироваться для системы, если нет, то можно вручную:

`locale-gen en_GB.UTF-8`

После этого добавить в конец `.bashrc`:

```bash
export LANGUAGE=en_GB.UTF-8
export LANG=en_GB.UTF-8
export LC_ALL=en_GB.UTF-8
```