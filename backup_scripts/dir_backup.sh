current_date=`date "+%Y-%m-%d-%H:%M:%S"`
LOG=/home/backup_hdd/backup_log.log
####################SRV####################
SRC=/srv/
DST=/home/backup_hdd/srv/
mkdir $DST

echo "!!!Starting backup for "$SRC" At date: "$current_date " !!!" >> $LOG
rsync --archive --one-file-system --progress --delete --link-dest=../Latest $SRC $DST/Processing-$current_date >> $LOG 2>&1
cd $DST
mv Processing-$current_date $current_date
rm -f Latest
ln -s $current_date Latest
printf "\\n\\n" >> $LOG

# Delete all folders and files that are older more than 15 days
# -ctime n
#               File's  status was last changed n*24 hours ago.  See the comments for -atime to understand how round‐
#               ing affects the interpretation of file status change times.
find $DST* -maxdepth 0 -ctime +4 -exec rm -r "{}" \; 