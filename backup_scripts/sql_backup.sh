# Date and backup path
backup_path="/backup/databases"
date=`date "+%Y-%m-%d-%H_%M_%S"`

# Set default file permissions
umask 177


########################################Backuping prometey########################################
# Database credentials
user="database_user"
password="db_password"
host="localhost"
db_name="_db_name_"

# Dump database into SQL file
cd $backup_path
mkdir $db_name
mysqldump --user=$user --password=$password --host=$host $db_name > $backup_path/$db_name/$db_name-$date.sql

# Compress databases using tar:
tar -c -v -j -f "$backup_path/$db_name/$db_name-$date.tar.bz2" "$backup_path/$db_name/$db_name-$date.sql"
rm $backup_path/$db_name/*.sql

# Delete all folders and files that are older more than 4 days
# -ctime n
#               File's  status was last changed n*24 hours ago.  See the comments for -atime to understand how round‐
#               ing affects the interpretation of file status change times.
find $backup_path/$db_name/* -maxdepth 0 -ctime +4 -exec rm -r "{}" \;
cd /home/nonroot/