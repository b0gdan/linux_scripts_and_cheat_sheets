# Cofigure security for SMB

## Enable SMB Encryption for the entire file server, type the following script on the server:
```shell
Set-SmbServerConfiguration –EncryptData $true
```

## Safeguarding the data for all clients that access the shares
```shell
Set-SmbServerConfiguration –RejectUnencryptedAccess $true
```

## Disabling SMB 1.0
```shell
Set-SmbServerConfiguration –EnableSMB1Protocol $false
```


Source: [SMB 3 Security Enhancements in Windows Server 2012](https://docs.microsoft.com/en-us/windows-server/storage/file-server/smb-security)

# Cofigure security for disks

## By default Bitlocker uses AES-128, but AES-256 provides stronger encryption.
## How enable it?


```shell
Windows Key + R
```

```shell
gpedit.msc
```

```shell
Computer Configuration\Administrative Templates\Windows Components\BitLocker Drive Encryption\Choose drive encryption method and cipher strength*
```

# Convert 128-bit AES Volumes to 256-bit AES Encryption


BitLocker doesn’t provide a way to convert existing BitLocker volumes to a different encryption method. You can do this yourself by decrypting the drive and then re-encrypting it with BitLocker. BitLocker will use 256-bit AES encryption when setting it up.


To do this, right-click an encrypted drive and select Manage BitLocker or navigate to the BitLocker pane in the Control Panel. Click the Turn off BitLocker link under an encrypted volume.


Allow Windows to decrypt the drive. When it’s done, re-enable BitLocker for the volume by right-clicking it and selecting Turn on BitLocker or clicking Turn on BitLocker in the Control Panel window. Go through the normal BitLocker setup process.


# Check Your BitLocker Volume’s Encryption Method


First, open a Command Prompt window as Administrator.


```shell
manage-bde -status
```


Source:


[How to Make BitLocker Use 256-bit AES Encryption Instead of 128-bit AES](https://www.howtogeek.com/193649/how-to-make-bitlocker-use-256-bit-aes-encryption-instead-of-128-bit-aes/)


