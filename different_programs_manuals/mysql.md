# Установка и работа с Postgres
```shell
/etc/init.d/mysql start
/etc/init.d/mysql stop
/etc/init.d/mysql restart
```

### How add remote user and database
Make sure line "skip-networking" is commented (or remove line) and add following line
`sudo nano /etc/mysql/mysql.conf.d/mysqld.cnf`
```shell
# bind-address = 192.168.2.33 ### 127.0.0.1 ### Add server ip to bind-address
sudo /etc/init.d/mysql restart
mysql --user root --password ### Enter MySql shell
SELECT User,Host FROM mysql.user; ### Проверка всех пользователей
SHOW DATABASES;
SHOW TABLES;
SHOW WARNINGS;
CREATE DATABASE nextcloud
DEFAULT CHARACTER SET utf8
DEFAULT COLLATE utf8_unicode_ci; ### Add a new database for remote access
CREATE USER 'nextcloud_user'@'localhost' IDENTIFIED BY 'password';
GRANT ALL ON nextcloud.* TO nextcloud_user@'localhost' IDENTIFIED BY 'password'; ### Звездочки в этой команде задают базу (prometey) и таблицу (* - все таблицы), соответственно, к которым у пользователя будет доступ. % - все хосты
GRANT ALL ON nextcloud.* TO nextcloud_user@'%' IDENTIFIED BY 'password'; ### Поле завершения настройки прав доступа новых пользователей, убедитесь, что вы обновили все права доступа
FLUSH PRIVILEGES;
DROP USER 'jeffrey'@'localhost'; # Удаление пользователя. Синтаксис команды: DROP USER [IF EXISTS] user [, user] ...
DROP DATABASE database_name; # Удаление базы данных DROP {DATABASE | SCHEMA} [IF EXISTS] db_name
mysql -u username -p database_name < file.sql ### Import database dump
mysqldump --user=_user_name_ --password=_strong_password_ --host=_host_address_ _database_name_ > _file_name_.sql
sudo ufw allow 3306/tcp ### Открыть порт для MySQL в firewall
sudo ufw status ### Проверить Firewall
netstat -an | grep "LISTEN" ### Еще раз проверить
sudo /etc/init.d/mysql restart ### Перезагрузить всё на всякий случай
### https://www.digitalocean.com/community/tutorials/mysql-ru

# Import csv to database table
mysqlimport \
--ignore-lines=1 \
--fields-terminated-by=, \
--local -u root \
-p Database \
TableName.csv
# To make the delimiter a tab, use:
--fields-terminated-by='\t'
```

Import sql dymp into DB:

`mysql -u username -p database_name < file.sql`