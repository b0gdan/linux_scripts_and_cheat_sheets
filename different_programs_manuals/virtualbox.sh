# Добавить поддержку флешек в virtualbox
# Добавить к текущему пользователю компа группу vboxusers
sudo adduser $USER vboxusers
#После перезагрузки вывод команды
grep vboxusers /etc/group
# Должен быть примерно такой
vboxusers:x:130:bogdan
#Важно, чтобы пользователь вконце был именно из-под которого идёт запуск virtualbox, иначе не будет видно usb-устройств.
#Плюч в настройках виртуалке включить поддержку именно второй версии USB и добавит пустой фильтр!!!
#ТАкже нужно установить на ХОСТ машину VirtualBox Extension Pack, который поддерживает работу с USB
xdg-open https://virtualbox.org/wiki/Downloads


usermod -a -G vboxusers $USER