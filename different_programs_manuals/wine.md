# Fedora case


This should install wine x64 and winetricks:

```shell
sudo dnf install winetricks.noarch --allowerasing
```

This should install wine x32:

```shell
dnf install wine-8.19-1.fc39.i686
```


GUI for wine:

https://github.com/winegui/WineGUI
