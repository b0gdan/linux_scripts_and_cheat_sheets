# Apache rewrite to remove index.php

Add parameter to config.php:

`'htaccess.RewriteBase' => '/',`

to my config.php, and made sure the two mods are enabled, also ran the

`a2enmod rewrite && a2enmod env`

`service apache2 restart`

`docker exec --tty --interactive --user 33 cloud.bogdan.co bash`

`php occ maintenance:update:htaccess`
