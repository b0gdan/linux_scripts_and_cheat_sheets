[Dust - Like du but more intuitive.](https://github.com/bootandy/dust)




Displays information regarding the amount of disk space used by the docker daemon:
```shell
docker container prune
```

You can reclaim a free space using the command line:
```shell
docker volume prune
```

Delete layers created during builds:
```shell
docker build prune
```

Prune the whole docker service:
```shell
docker system prune --all --volumes
```

Delete SystemD logs:

2. If you decide to clear the logs, run command to rotate the journal files. All currently active journal files will be marked as archived, so that they are never written to in future.

sudo journalctl --rotate

3. Now clear the journal logs by choosing one of following commands:

    Delete journal logs older than X days:

    sudo journalctl --vacuum-time=2days

    Delete log files until the disk space taken falls below the specified size:

    sudo journalctl --vacuum-size=100M

    Delete old logs and limit file number to X:

    sudo journalctl --vacuum-files=5

https://ubuntuhandbook.org/index.php/2020/12/clear-systemd-journal-logs-ubuntu/
