# For one domain
wget https://dl.eff.org/certbot-auto
chmod a+x certbot-auto
./certbot-auto certonly --authenticator manual

# For wildcart
wget https://dl.eff.org/certbot-auto
chmod a+x certbot-auto
./certbot-auto certonly --manual -d *.bogdan.co -d bogdan.co --preferred-challenges dns-01 --server https://acme-v02.api.letsencrypt.org/directory
# Check DNS records for validatind
watch dig -t txt _acme-challenge.bogdan.co

#To non-interactively renew *all* of your certificates, run
./certbot-auto renew
#View cert info
openssl x509 -text -in cert.pem

# Converting linux cert to indows
openssl pkcs12 -export -out certificate.pfx -inkey privkey.pem -in cert.pem -certfile chain.pem

#Check Certificate Serial Number
openssl x509 -serial -noout -in CERTIFICATE_FILE
#Check Thumbprint (fingerprint) Serial Number
openssl x509 -fingerprint -noout -in CERTIFICATE_FILE
openssl x509 -noout -fingerprint -sha1 -inform pem -in CERTIFICATE_FILE
openssl x509 -noout -fingerprint -sha256 -inform pem -in CERTIFICATE_FILE
openssl x509 -noout -fingerprint -md5 -inform pem -in CERTIFICATE_FILE
