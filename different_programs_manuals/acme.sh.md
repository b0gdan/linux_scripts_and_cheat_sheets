# Manual for acme.sh

Project repo: https://github.com/Neilpang/acme.sh

Issue certificate automatically:

```bash
cd /home/bogdan
.acme.sh/acme.sh --upgrade
export CF_Key="cloudflare_api_key_from_keepass"
export CF_Email="b0gdan@protonmail.com"

.acme.sh/acme.sh  --issue \
-d prometey.io -d '*.prometey.io' \
-d bogdan.co  -d '*.bogdan.co' \
-d bcgprometey.ru  -d '*.bcgprometey.ru' \
--dns dns_cf

.acme.sh/acme.sh  --issue \
-d buh.prometey.io \
-d chat.bogdan.co  -d '*.chat.bogdan.co' \
--dns dns_cf
```

Check certificate info:
```bash
openssl x509 -in bcgprometey.ru/bcgprometey.ru.cer -text -noout
```

Manually issue wildcart certificate to the domain name:

`.acme.sh/acme.sh --issue --dns -d bogdan.co  -d '*.bogdan.co' --yes-I-know-dns-manual-mode-enough-go-ahead-please`

Check inserted DNS txt values:

`.acme.sh/acme.sh --renew --dns -d bogdan.co  -d '*.bogdan.co' --yes-I-know-dns-manual-mode-enough-go-ahead-please`
