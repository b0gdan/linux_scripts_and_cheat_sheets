﻿#Подробности:
xdg-open https://putty.org.ru/articles/fail2ban-ssh.html
# Installing
apt install fail2ban

#Конфигурация Fail2ban
#У программы два основных файла конфигурации:
nano /etc/fail2ban/fail2ban.conf #отвечает за настройки запуска процесса Fail2ban
nano /etc/fail2ban/jail.conf #содержит настройки защиты конкретных сервисов, в том числе sshd
fail2ban-client status sshd

#Файл jail.local поделён на секции, так называемые «изоляторы» (jails), каждая секция отвечает за определённый сервис и тип атаки:
[DEFAULT]
action = %(action_mwl)s
backend = systemd
banaction = firewallcmd-ipset
# то банить IP на bantime = 28 days
bantime  = 2419200
destemail = i@bogdan.co
# если в течении findtime = 1 day
findtime = 86400
## Постоянный IP-адрес.
## Если не переопределить ignoreip здесь,
## то стоит закомментировать этот параметр в jail.conf.
ignoreip = 8.8.8.8
# произведено 3 неудачных попыток логина:
maxretry = 3
sender = lvv@bcgprometey.ru
# Банить по всем протоколам
protocol = all

[sshd]
enabled = true
port    = 2022
