рекомендую msmtp, он в отличие от msmtp не заброшен автором и прекрасно работает

#Устанавливваем msmtp
install msmtp msmtp-mta ca-certificates
#Устанавливваем модуль для php
apt-get install php-mail

#Once these are installed, a default config is required. By default msmtp will look at /etc/msmtprc
nano /etc/msmtprc


##################################КОНФИГУРАЦИЯ msmtp##################################
# Set default values for all following accounts.
defaults
auth           on
tls            on
tls_starttls on
tls_trust_file /etc/ssl/certs/ca-certificates.crt
logfile /var/log/msmtp/msmtp.log

# Yandex
account yandex
host smtp.yandex.ru
port 587
auth on
user noreply@bcgprometey.ru
password <password>
from noreply@bcgprometey.ru

# Yandex
account google
auth on
host smtp.gmail.com
port 587
auth on
user bogdan.lashkov@gmail.com
password <password>
from bogdan.lashkov@gmail.com


# Set a default account
account default : yandex
##################################КОНФИГУРАЦИЯ msmtp##################################

#As there are passwords in config file, it shouldn't be available for reading by anyone except root user
chmod 600 msmtprc

#Now we should make directory for logging
nano /etc/logrotate.d/msmtp
##################################КОНФИГУРАЦИЯ /etc/logrotate.d/msmtp##################################
/var/log/msmtp/*.log {
	rotate 12
	monthly
	compress
	missingok
	notifempty
}
##################################КОНФИГУРАЦИЯ /etc/logrotate.d/msmtp##################################
sudo mkdir /var/log/msmtp
touch /var/log/msmtp/msmtp.log
sudo chown -R www-data:root /var/log/msmtp

#all below is Optional!!! I don't know why, but all works without this config:
#Now that the logging is configured, we need to tell PHP to use msmtp by editing
nano /etc/php/7.1/fpm/php.ini
#and updating the sendmail path from
sendmail_path =
#to
sendmail_path = "/usr/bin/msmtp -C /etc/msmtprc -a <MSMTP_ACCOUNT_NAME> -t"



##################################TESTING##################################
php -r "mail('i@bogdan.co', 'Test', 'Test');"
echo "hello there username." | msmtp -a google i@bogdan.co
echo "Subject: Test This is a test mail" | msmtp -a yandex --debug --from=noreply@bcgprometey.ru -t i@bogdan.co




xdg-open https://blog.joeb454.com/2016/08/configuring-msmtp-on-ubuntu-16-04/