## Различные комманды
### Установить Docker
`xdg-open https://docs.docker.com/install/linux/docker-ce/ubuntu/`

### Сохранить изменения в контейнере:
`sudo docker commit <container_id> image_name`
### Запустить:
`docker run --tty --interactive --publish-all ubuntu_bogdan`

### Set static IP to the docker container
`docker network create --subnet=172.18.0.0/16 dockernet`

`docker run --net mynet123 --ip 172.18.0.22 -it ubuntu bash`

### Connect to the running container
`docker exec -it [container-id] bash`

### Чтобы запускать Docker контейнеры под своим пользователем (без sudo), нужно добавиться в соответствующую группу:
`sudo usermod -aG docker YOU_USER`

### Ещё про docker
`xdg-open http://onedev.net/post/579`

### Остановить/удалить все контейнеры
`docker rm $(docker ps -a -q)`
`docker stop $(docker ps -a -q)`
`docker rmi $(docker images -a -q)`

###  Вывести только имена работающих контейнеров
`docker ps | awk '{print $2"\t"}'`

### Просмотреть логи по проблемному контейнеру
`docker logs --tail 50 --follow --timestamps cloud.bogdan.co`



## Работа с сетью
###  Создание сети
docker network create --subnet=172.18.0.0/24 homesrv_doc_net
###  Как сеть используется контейнерами
docker network inspect homesrv_doc_net
###  Пример
```
docker run \
--restart unless-stopped \
--detach \
--net homesrv_doc_net \
--ip 172.18.0.3 \
--name cloud.bogdan.co \
--volume /srv/cloud.bogdan.co:/var/www/html \
--link some-redis:redis \
--link document.bogdan.co:onlyoffice/documentserver \
nextcloud
```

#№ Docker - How to cleanup (unused) resources

Once in a while, you may need to cleanup resources (containers, volumes, images, networks) ...

### delete volumes

// see: https://github.com/chadoe/docker-cleanup-volumes

```
$ docker volume rm $(docker volume ls -qf dangling=true)
$ docker volume ls -qf dangling=true | xargs -r docker volume rm

```
### delete networks
```

$ docker network ls  
$ docker network ls | grep "bridge"   
$ docker network rm $(docker network ls | grep "bridge" | awk '/ / { print $1 }')
```
### remove docker images

// see: http://stackoverflow.com/questions/32723111/how-to-remove-old-and-unused-docker-images
```
$ docker images
$ docker rmi $(docker images --filter "dangling=true" -q --no-trunc)
```

```
$ docker images | grep "none"
$ docker rmi $(docker images | grep "none" | awk '/ / { print $3 }')
```

### remove docker containers

// see: http://stackoverflow.com/questions/32723111/how-to-remove-old-and-unused-docker-images
```
$ docker ps
$ docker ps -a
$ docker rm $(docker ps -qa --no-trunc --filter "status=exited")
```

### Resize disk space for docker vm
```
$ docker-machine create --driver virtualbox --virtualbox-disk-size "40000" default
```
