рекомендую msmtp, он в отличие от ssmtp не заброшен автором и прекрасно работает

#Устанавливваем ssmtp
apt-get install ssmtp
#Устанавливваем модуль для php
apt-get install php-mail

#Полный список параметров
man -m 5 ssmtp.conf

#Делаем бекап файлов конфигуации
cp /etc/ssmtp/ssmtp.conf /etc/ssmtp/ssmtp.conf.default
cp /etc/ssmtp/revaliases /etc/ssmtp/revaliases.default

#Редактируем файл конфигурации
nano /etc/ssmtp/ssmtp.conf



##################################КОНФИГУРАЦИЯ SSMTP##################################
#
# Config file for sSMTP sendmail
#
# The person who gets all mail for userids < 1000
# Make this empty to disable rewriting.
# А также вы можете указать нужный е-майл на который будет
# приходить почта для root.
root=bogdan

# The place where the mail goes. The actual machine name is required no
# MX records are consulted. Commonly mailhosts are named mail.domain.com
mailhub=smtp.yandex.ru:465
# Указываем имя пользователя на smtp-сервере (от какого пользователя шлем почту).
AuthUser=lvv@bcgprometey.ru
# Указываем пароль от smtp-аккаунта:
AuthPass=secret_password
AuthMethod=LOGIN
# Использовать SSL/TLS, чтобы отправить безопасные сообщения на сервер.
# Должно присутсвовать обязательно, иначе не пройдет авторизация Yandex:
UseTLS=YES
# Использовать SSL/TLS, чтобы отправить безопасные сообщения на сервер.
#Должно присутсвовать обязательно, иначе не пройдет авторизация Gmail:
#UseSTARTTLS=YES
# Использовать SSL/TLS сертификат для аутентификации на SMTP-хосте.
#UseTLSCert=YES

# Используйте этот сертификат RSA.
#TLSCert=/usr/local/etc/ssmtp/ssmtp.pem

# Получить расширенное (* действительно * расширенную) отладочную информацию в логах
# Если вы хотите иметь отладке в конфигурационных отпарсенных файлах, переместите этот
# параметр в начало файла конфигурации и раскомментируйте
# Логи хранятся тут: tail -f /var/log/mail.log
Debug=YES

# Where will the mail seem to come from?
#если используете pdd.yandex.ru можно указать свой домен mydomain.ru
rewriteDomain=bcgprometey.ru

# The full hostname
hostname=bcgprometey.ru

# Are users allowed to set their own From: address?
# YES - Allow the user to specify their own From: address
# NO - Use the system generated From: address
FromLineOverride=YES
##################################КОНФИГУРАЦИЯ SSMTP##################################


# В данном конфиге явно указывается, почта от какого пользователя и с какого ящика может уходить.
# Данная настройка позволяет защитить сервер от возможности слать через него спам с пользовательских учеток.
# для яндекса:
##################################КОНФИГУРАЦИЯ revaliases##################################
# sSMTP aliases
#
# Format:       local_account:outgoing_address:mailhub
#
# Example: root:your_login@your.domain:mailhub.your.domain[:port]
# where [:port] is an optional port number that defaults to 25.
bogdan:lvv@bcgprometey.ru:smtp.yandex.ru:465
www-data:lvv@bcgprometey.ru:smtp.yandex.ru:465 #Чтобы письма мог отправлять и наш web-сервер, инаяче будет 504 error php ssmpt
root:lvv@bcgprometey.ru:smtp.yandex.ru:465
nonroot:lvv@bcgprometey.ru:smtp.yandex.ru:465
##################################КОНФИГУРАЦИЯ revaliases##################################


# В *NIX-мире для передачи почты первым был написан сервер sendmail. Этот сервер долгое время был стандартом де-факто. Постепенно была сформирована концепция MTA — то есть, концепция Mail Transfer Agent. MTA — это элемент системы передачи почты, который занимается только процессом передачи почтовых сообщений.
# Однако исторически в системе Linux принято, что программа для передачи почтовых сообщений должна быть именно sendmail. Поэтому, когда устанавливается любая из MTA-программ, она создает файлы линков /usr/sbin/sendmail и /usr/lib/sendmail, которые указывают на нужную программу.

# Посмотрим куда у нас ведет mail
whereis mail
#mail: /etc/mail
#Проверяем файл симилинк:
ls -l /usr/sbin | grep sendmail
#Должны увидеть
#lrwxrwxrwx 1 root root        5 апр 13  2016 sendmail -> ssmtp
#Если же нет то виртуальный пакет mail-transfer-agent не ссылается на пакет ssmtp, и это необходимо исправить. Удалив пакеты на которые ссылается mail-transfer-agent, к примеру posfix. 
#Если нужно настроить отправку писем для своего сайта, использующего php, сделайте следующие изменения в файле php.ini, который может находится в разных каталогах в зависимости от способа подключения интерпретатора php:
nano /etc/php/7.0/fpm/php.ini
#Ищем строку sendmail_path и прописываем:
sendmail_path = /usr/sbin/ssmtp -t

#Перезапускаем PHP и NGINX
sudo service php7.0-fpm restart
sudo service nginx restart

#Проверяем работу ssmtp через консоль:
echo "Это тестовое письмо из консоли" | ssmtp -v -s i@bogdan.co

#Если всё ок, то разрешаем смопользователям, которые должны отправлять почту, например выставляем такие права в конфиг /etc/ssmtp/ssmtp.conf:
chmod 644 ssmtp.conf

#Для проверки сделанных настроек создайте файл в корневой директории вашего сервера вида phpinfo.php и вставьте в него следующий код:
    
<?php
    mail('i@bogdan.co', 'Заработало', 'Это проверка работы ssmtp');
    phpinfo();
?>
#Соответственно, на указанный ящик вы должны получить письмо.
#А в информации о PHP должно появиться слудующее
#sendmail_path	/usr/sbin/ssmtp -t	/usr/sbin/ssmtp -t




#http://itautsors.ru/ssmtp-nastroyka-otpravki-pochty-s-ubuntudebianlinux
#http://prosto-tak.ru/ssmtp-i-yandex-pochta/
