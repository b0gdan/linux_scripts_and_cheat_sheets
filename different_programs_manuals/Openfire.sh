wget -c --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/8u144-b01/090f390dda5b47b9b721c7dfaa008135/jre-8u144-linux-x64.tar.gz


xdg-open http://prosody.im/doc/dns
xdg-open https://support.cloudflare.com/hc/en-us/articles/200168726-How-do-I-add-SRV-records-

#############Some theory#############
SRV records essentially allow transparent DNS-level redirects of XMPP services to another domain or port. A simple example is when you want users to have addresses like username@example.com, but your XMPP server is really installed on xmpp.example.com. In principle they work the same way as MX records do for email.

SRV records are not XMPP-specific, but are defined for a number of protocols. XMPP actually has 2 different types of SRV records, those for clients to use (client-to-server, or 'c2s'), and those for other XMPP servers to use when they look up your domain (server-to-server, or 's2s').

Following the previous example of a server 'example.com' wanting to delegate its XMPP services to the server at 'xmpp.example.com', here are some example records:

_xmpp-client._tcp.example.com. 18000 IN SRV 0 5 5222 xmpp.example.com.
_xmpp-server._tcp.example.com. 18000 IN SRV 0 5 5269 xmpp.example.com.



#############CloudFlare setup#############
Using the examples below to guide you, enter the record details into Cloudflare DNS

Note: Cloudflare combines the Service and Protocol fields you enter into SRV record name.

EXAMPLE ONE

`_xmpp-client._tcp.yourdomain.com. IN SRV 5 0 5222 talk.l.google.com.`

Service: _xmpp-client
Protocol: tcp
Name: yourdomain.com

Select the green check-mark

Priority: 5
Weight: 0
Port: 5222
Target: talk.l.google.com


#Добавление поддержки русских символов в БД MySQL для Openfire
nano /etc/mysql/mysql.conf.d/mysqld.cnf
#В конец секции mysqld добавить
collation_server=utf8_unicode_ci
character_set_server=utf8
skip-character-set-client-handshake

$openfire_dir/bin/openfire restart

# Создать новую БД
