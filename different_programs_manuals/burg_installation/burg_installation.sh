# burg installation
add-apt-repository ppa:n-muench/burg
apt-get update
# This will install the burg bootloader 
apt-get install burg burg-themes
# Select "(sd0)" with your corresponding hard disk: "(sd1)", "(sd2)" etc.
# You will be prompted with options while installation. press Tab to switch to OK button and hit Enter
# In the next screen select the path for the bootloader to install with space and hit tab and Enter.
# Во время установки Burg будут появляться окна с вопросами, если вы не знаете что ответить, просто нажимайте на Enter до следующего окна:
# Здесь нужно нажать на клавишу Пробел, после чего должна появиться звездочка в скобках для выбора дискового устройства для установки, как на верхнем снимке и вновь Enter.
burg-install "(sd0)"
# Обновите BURG
update-burg
# Настроить BURG, не выходя из системы, открыв его в эмуляторе следующей командой:
burg-emu
# Клавиша F1 открывает меню помощи