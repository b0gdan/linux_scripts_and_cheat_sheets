# Установка и работа с Postgres
[Official link for installing postgres](https://www.postgresql.org/download/linux/ubuntu/)

Switch over to the postgres account on your server by typing

`sudo -i -u postgres`

You can now access a Postgres prompt immediately by typing:

`psql`

Some psql commands:
```
\list # list all databases
\l # list all databases
\connect <database_name> #To switch databases
\connect <database_name> --Use database
\du # List user accounts
\password <username> # Change password
\q # quit
\conninfo #Once logged in, you can get check your current connection information
\d --list down all the tables in an attached database.
```
Create a New Role

If you are logged in as the postgres account, you can create a new user by typing:

`postgres@server:~$ createuser --interactive`

`createuser --no-createdb --no-createrole --no-superuser --password <username>`

`dropuser <username>`

It is highly recommended to add new super user for postgres in maintaince aims. And change password only only for this account!

Create a New Database

`postgres@server:~$ createdb <dbname>`

Allowing local connections

`nano /etc/postgresql/<postgres_version>/main/pg_hba.conf` # Ubuntu/Debian 

`nano /var/lib/pgsql/10/data/pg_hba.conf` # CentOS

Add acces in the following format
```
# TYPE  DATABASE        USER            ADDRESS                 METHOD
# "local" is for Unix domain socket connections only
local   all                         all                                                  peer
# IPv4 local connections:
host    all                         all                          127.0.0.1/32            ident
host    onlyoffice                  onlyoffice_user              172.18.0.15/32          md5
host    nextcloud                   nextcloud_user               192.168.2.13/32           md5
host    nextcloud                   nextcloud_user               172.18.0.3/32           md5
host    prometey_landing            prometey_landing_user        172.18.0.11/32           md5
# IPv6 local connections:
host    all             all             ::1/128                 ident
# Allow replication connections from localhost, by a user with the
# replication privilege.
local   replication     all                                     peer
host    replication     all             127.0.0.1/32            ident
host    replication     all             ::1/128                 ident
```

Some useful SQL — adminning commands
```
alter user <username> with encrypted password '<password>';
grant all privileges on database <dbname> to <username> ;
```

Import SQL dump file:

`psql databasename < data_base_dump.sql`



# Migrate from old version to a new one:

Show which clusters do we have:

```shell
pg_lsclusters
```

Migrate data:

```shell
pg_dumpall -p 54320 >backup.sql
pg_dumpall -p 54320 | psql -d postgres -p 5432
```

Change setting
```shell
diff /etc/postgresql/12/main/postgresql.conf /etc/postgresql/14/main/postgresql.conf | less
```

Remove old instance 

```shell
apt-get --purge autoremove  postgresql-12 postgresql-client-12
rm -rf  /etc/postgresql/12
rm -rf  /var/lib/postgresql/12
```
