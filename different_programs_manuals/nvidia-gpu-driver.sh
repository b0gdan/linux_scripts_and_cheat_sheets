### Driver installation on Fedora

#### Add non-free nvidia repository:
https://rpmfusion.org/Howto/NVIDIA

#### Update the system
sudo dnf upgrade --refresh

sudo dnf update -y # and reboot if you are not on the latest kernel


sudo dnf install akmod-nvidia

xdg-open https://bugs.launchpad.net/ubuntu-gnome/+bug/1559576 # Bug with black screen
apt install xserver-xorg-legacy

#### Nvidia Setting
nvidia-smi

#### Check current renderer
glxinfo | egrep "OpenGL vendor|OpenGL renderer"


#### Instructions for dual graphics on Fedora and Gnome:

* EnvyControl
* GPU profile selector (https://extensions.gnome.org/extension/5009/gpu-profile-selector/)

https://fostips.com/install-nvidia-driver-fedora-36/