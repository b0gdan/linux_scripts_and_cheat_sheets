##########################################warning: No server certificate verification method enabled##########################################
#Put this in .ovpn file:
remote-cert-tls server
remote-cert-ku f6
# The problem here is that OpenVPN expects this special field in the certificate structure (ku) to be set to a certain value. This value is usually used for certificates that are to be used for VPN servers. It makes sense to check this, because if the field is not correct, it is possible that some client (not a VPN server), has gotten a certificate from the same certificate authority that the VPN server itself, and is trying to impersonate the server.
# The provided workaround tells OpenVPN not to expect the field to be set correctly, and instead accept the value f6, which is what SoftEther puts in it's own generated certificates.
http://www.vpnusers.com/viewtopic.php?f=7&t=2622&sid=6b36384386fc436cf86593f3d05568a9#top


##########################################OpenVPN 443 TLS port##########################################
#edit .ovpn file with a texteditor and change
proto udp --to--> proto tcp #(udp is also possible, but sometimes blocked by firewalls)
#and of course:
remote openvpn_host.example.com 1194 --to--> remote openvpn_host.example.com 443



# Enable service on boot
### 1. Create file in:
nano /etc/systemd/system/vpnserver.service



######
[Unit]
Description=SoftEther VPN Server
After=network.target

[Service]
Type=forking
ExecStart=/usr/local/vpnserver/vpnserver start
ExecStop=/usr/local/vpnserver/vpnserver stop

[Install]
WantedBy=multi-user.target
######


systemctl enable vpnserver.service


# Programs to show traffic:
apt install bmon
apt install iftop

###############OPENVPN ON LINUX PC AND ANDROID###############
# On PC:
apt install openvpn network-manager-openvpn
# Then you should import *.ovpn config file, using system settings "import VPN connection..."
# On Android:
# Install OpenVPN for Android
# Import *.ovpn config file, simple things!

###############FIXING DNS LEAKS###############

Go to "System Settings"
Click on "Network" 
Select the NON-VPN connection 
Click "Configure" 
Click on "Method" and set it to "Automatic (DHCP) addresses only" 
Enter our DNS servers ( 209.222.18.222 and 209.222.18.218 ) 
Click Save

Adding following lines into the .ovpn file before I import it to Network Manager on Ubuntu fixed DNS leak with my VPN connection established by NetworkManager.
These lines should be added below the lines of initial commands and before the lines of certificates.

script-security 2
up /etc/openvpn/update-resolv-conf
down /etc/openvpn/update-resolv-conf
