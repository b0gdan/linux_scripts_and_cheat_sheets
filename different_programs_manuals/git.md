# Git cheat sheet
### Add files in project to git recursively
`git add --all`

### Pull repo:
`ssh-agent bash -c 'ssh-add /home/bogdan/Programs/ssh/gitlab.mobbtech.com; git clone git@gitlab.mobbtech.com:mobile/analyze.git'`

### Update your current HEAD branch with the latest changes from the remote server:
`ssh-agent bash -c 'ssh-add /home/bogdan/Programs/ssh/gitlab.mobbtech.com; git pull origin master'`

### Create the branch on your local machine and switch in this branch:
`git checkout -b [name_of_your_new_branch]`

### Commit all local changes
`git commit -m "some message goes here"`

`git commit -am "Started work on models and migrations"`

### Push the branch on git hub (Private key is not in standart dir):
`ssh-agent bash -c 'ssh-add /home/bogdan/Programs/ssh/gitlab.mobbtech.com; git checkout -b git push origin [name_of_your_new_branch]'`

### Show all commits history
`git reflog`

### Switch to the nessesary commit (WITHOUT deleting files)
git reset --soft `

### Switch to the nessesary commit (WITH deleting files)
`git reset --hard <hash_from_reflog>`

### Add remote ropository
`git remote add origin https://git.example.com/user_name/repo_name.git`

### Push made changes (after commit)
`git push -u origin master`


`git config --global user.name "Bogdan Lashkov"`
`git config --global user.email "user@mail.com"`
`mkdir -p ~/git/testing ; cd ~/git/testing`
`git init ##### Make empty repository`
`git clone http://widl.rz.tu-ilmenau.de/thomas/tvd-backend.git ##### How clone master branch`
`git clone -b existing_branch_name http://widl.rz.tu-ilmenau.de/thomas/tvd-backend.git ##### How clone some branch`
`git checkout -b new_branch_name ##### Making new branch`

`git remote ##### Shows all remote URLs`
`git fetch tvdbackend_repo ### if you want to fetch all the information that Paul has but that you don’t yet have in your`
`git ls-tree -r reporting_service`
`git diff --name-only reporting_service`