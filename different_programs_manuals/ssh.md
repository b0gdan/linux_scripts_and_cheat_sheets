# Connecting to the remote servers with keys

## Check Available SSH Keys on Your Computer
```shell script
for key in ~/.ssh/*; do ssh-keygen -l -f "${key}"; done | uniq
```

* 📛 DSA: It’s unsafe and even no longer supported since OpenSSH version 7, you need to upgrade it!
* ⚠️ RSA: It depends on key size. If it has 3072 or 4096-bit length, then you’re good. Less than that, you probably want to upgrade it. The 1024-bit length is even considered unsafe.
* 👀 ECDSA: It depends on how well your machine can generate a random number that will be used to create a signature. There’s also a trustworthiness concern on the NIST curves that being used by ECDSA.
* 🔒 Ed25519: It’s the most recommended public-key algorithm available today!

## Generating Ed25519 Key

Open up your terminal and type the following command to generate a new SSH key that uses Ed25519 algorithm:

```shell script
ssh-keygen -o -a 256 -t ed25519 -f ~/.ssh/id_ed25519 -C "i@bogdan.co"
```

The same, but less secure for fucking Microsoft:
```shell script
ssh-keygen -o -a 512 -t rsa-sha2-512 -f ~/.ssh/dev.azure.com -C "info@finelbo.com"
```


You’ll be asked to enter a passphrase for this key, use the strong one. You can also use the same passphrase like any of your old SSH keys.

* `-o` : Save the private-key using the new OpenSSH format rather than the PEM format. Actually, this option is implied when you specify the key type as `ed25519`.
* `-a`: It’s the numbers of KDF (Key Derivation Function) rounds. Higher numbers result in slower passphrase verification, increasing the resistance to brute-force password cracking should the private-key be stolen.
* `-t`: Specifies the type of key to create, in our case the Ed25519.
* `-f`: Specify the filename of the generated key file. If you want it to be discovered automatically by the SSH agent, it must be stored in the default `.ssh` directory within your home directory.
* `-C`: An option to specify a comment. It’s purely informational and can be anything. But it’s usually filled with `<login>@<hostname>` who generated the key.

You can find your newly generated private key at `~/.ssh/id_ed25519` and your public key at `~/.ssh/id_ed25519.pub`.

```shell script
/$HOME/.ssh/id_ed25519			#Private key Elliptic Curve Digital Signature Algorithm
/$HOME/.ssh/id_ed25519.pub		#Public key Elliptic Curve Digital Signature Algorithm
cat /$HOME/.ssh/id_ed25519.pub 		#Later copy paste this public key to the server/traget
```
Make the SSH key pair folder on the client side only accessible for the local `$USER`. Note that root and your `$USER` username have different directories for storing generated SSH key pairs. Best practices dictate to leverage the `$USER` directory when generating your SSH key pair:
```shell script
#Make the .ssh directory unreadable for other users and groups
chmod 700 ~/.ssh			
chmod 700 "/$HOME/.ssh"	
#Make the private SSH key read only
chmod 400 /$HOME/.ssh/id_ed25519 
chmod 400 ~/.ssh/id_ed25519 				 	    	
#Make the local $USER own the SSH key pair files
chown -R -c $USER:$USER ~/.ssh
```


## Adding Your Key to the server
On the server side we set the correct permissions and copy the public key to the `authorized_keys` file:

Always remember that your public key is the one that you copy to the target host for authentication.

```shell script
rm /etc/ssh/ssh_host_* 					#Delete old SSH keys
rm ~/.ssh/id_* 						#Delete old SSH keys
sudo dpkg-reconfigure openssh-server				#Reset SSH config to defaults and generate new key files
rm /home/$USER/.ssh/id_*     					#Delete old SSH keys
vi /home/$USER/.ssh/authorized_keys				#paste public key here
cd /home/$USER/ && chmod g-w,o-w .ssh/			#The directory containing your .ssh directory must not be writeable by group or others
chmod 600 /home/$USER/.ssh/authorized_keys			#change permissions to r+w only for user
service sshd restart						#restart and reload keys into the SSH deamon
```

## Adding Your Key to SSH Agent

Before adding your new private key to the SSH agent, make sure that the SSH agent is running by executing the following command:

```shell script
eval "$(ssh-agent -s)"
```

Then run the following command to add your newly generated Ed25519 key to SSH agent:

```shell script
ssh-add ~/.ssh/id_ed25519
```

Or if you want to add all of the available keys under the default .ssh directory, simply run:
```shell script
ssh-add
```


## Lets test the authentication from the client to the server
```shell script
ssh USER@ssh-server-ip -i /$HOME/.ssh/id_ed25519 -o PasswordAuthentication=no -vv
```

## Simplify connecting command
Add an entry to the `~/.ssh/config` file to configure these options:
```yaml
Host awesome
  HostName 198.222.111.33
  User john
  IdentityFile ~/.ssh/id_ed25519
  IdentitiesOnly yes
```

Example:
```yaml
# GitLab.com
Host gitlab.com
  PreferredAuthentications publickey
  IdentityFile ~/.ssh/gitlab.com

# Oracle cloud
Host ffm.bogdan.co
  HostName ffm.bogdan.co
  User bogdan
  IdentityFile ~/.ssh/ffm.bogdan.co
  IdentitiesOnly yes
  PubKeyAuthentication yes
```


## Add the public key to the server
```shell script
cat ~/.ssh/id_ed25519.pub | ssh user@123.45.56.78 "cat >> ~/.ssh/authorized_keys"
```

## Testing connection
```shell script
ssh -T git@gitlab.com
```

# Sources
* [How to secure your SSH server with public key Ed25519 Elliptic Curve Cryptography](https://cryptsus.com/blog/how-to-secure-your-ssh-server-with-public-key-elliptic-curve-ed25519-crypto.html)
* [Upgrade Your SSH Key to Ed25519](https://medium.com/risan/upgrade-your-ssh-key-to-ed25519-c6e8d60d3c54)
