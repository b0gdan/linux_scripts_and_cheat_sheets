#How to get connections from another static ip

Create systemd unit file:

```bash
nano /usr/lib/systemd/system/secure-tunnel@.service 
```


Its content:
```bash
[Unit]
Description=Setup a secure tunnel to %I
After=network-online.target ssh.service

[Service]
User=root

ExecStart=/usr/bin/ssh -F /root/.ssh/config -NT %i

# Restart every >2 seconds to avoid StartLimitInterval failure
RestartSec=5
Restart=always

[Install]
WantedBy=multi-user.target
```


Content of `/root/.ssh/config`:

```bash
Host ffm.bogdan.co
  HostName ffm.bogdan.co
  User root
  IdentityFile /root/.ssh/ffm.bogdan.co
  IdentitiesOnly yes
  RemoteForward localhost:2222 localhost:22
  RemoteForward ffm.bogdan.co:80 localhost:80
  RemoteForward ffm.bogdan.co:443 localhost:443
  RemoteForward ffm.bogdan.co:5432 localhost:5432
  ServerAliveInterval 60
  ExitOnForwardFailure yes
```


```
systemctl enable secure-tunnel@ffm.bogdan.co.service
systemctl start secure-tunnel@ffm.bogdan.co.service
systemctl status secure-tunnel@ffm.bogdan.co.service
```
