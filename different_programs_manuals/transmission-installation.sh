﻿###Настройка web-интерфейса для transmission
#Установить всё необходимое
apt-get install transmission-cli transmission-common transmission-daemon transmission
#Разрешить 9092 порт
ufw allow 9092/tcp
#Проверить
ufw status
#Остановить демон transmission для его кофигурации
service transmission-daemon stop
#Проверить
service transmission-daemon status
#Настроить
{
    "alt-speed-down": 50,
    "alt-speed-enabled": false,
    "alt-speed-time-begin": 540,
    "alt-speed-time-day": 127,
    "alt-speed-time-enabled": false,
    "alt-speed-time-end": 1020,
    "alt-speed-up": 50,
    "bind-address-ipv4": "0.0.0.0",
    "bind-address-ipv6": "::",
    "blocklist-enabled": false,
    "blocklist-url": "",
    "cache-size-mb": 4,
    "dht-enabled": true,
    "download-dir": "/home/nonroot/Downloads/transmission-download-dir",
    "download-limit": 100,
    "download-limit-enabled": 0,
    "download-queue-enabled": true,
    "download-queue-size": 5,
    "encryption": 1,
    "idle-seeding-limit": 30,
    "idle-seeding-limit-enabled": false,
    "incomplete-dir": "/home/nonroot/Downloads/transmission-incomplete-dir",
    "incomplete-dir-enabled": false,
    "lpd-enabled": false,
    "max-peers-global": 200,
    "message-level": 2,
    "peer-congestion-algorithm": "",
    "peer-id-ttl-hours": 6,
    "peer-limit-global": 200,
    "peer-limit-per-torrent": 50,
    "peer-port": 51413,
    "peer-port-random-high": 65535,
    "peer-port-random-low": 49152,
    "peer-port-random-on-start": false,
    "peer-socket-tos": "default",
    "pex-enabled": true,
    "port-forwarding-enabled": false,
    "preallocation": 1,
    "prefetch-enabled": true,
    "queue-stalled-enabled": true,
    "queue-stalled-minutes": 30,
    "ratio-limit": 2,
    "ratio-limit-enabled": false,
    "rename-partial-files": true,
    "rpc-authentication-required": true,
    "rpc-bind-address": "192.168.2.33",
    "rpc-enabled": true,
    "rpc-password": "{d61d8e5697a7f5fbce152d1e92bf596246ae770ay4hn.3CE",
    "rpc-port": 9092,
    "rpc-url": "/transmission/",
    "rpc-username": "bogdan",
    "rpc-whitelist": "*.*.*.*",
    "rpc-whitelist-enabled": false,
    "scrape-paused-torrents-enabled": true,
    "script-torrent-done-enabled": false,
    "script-torrent-done-filename": "",
    "seed-queue-enabled": false,
    "seed-queue-size": 10,
    "speed-limit-down": 100,
    "speed-limit-down-enabled": false,
    "speed-limit-up": 100,
    "speed-limit-up-enabled": false,
    "start-added-torrents": true,
    "trash-original-torrent-files": false,
    "umask": 18,
    "upload-limit": 100,
    "upload-limit-enabled": 0,
    "upload-slots-per-torrent": 14,
    "utp-enabled": true
}
#Создать папки
mkdir /home/nonroot/Downloads/transmission-download-dir /home/nonroot/Downloads/transmission-incomplete-dir
#Дать права
chown -R -v debian-transmission:debian-transmission transmission-download-dir transmission-incomplete-dir
#Запустить заново
service transmission-daemon start