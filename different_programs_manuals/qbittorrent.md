# Qbittorrent

https://github.com/linuxserver/docker-qbittorrent

Prepare docker container:
```bash
docker create \
--name=qbittorrent \
-e PUID=1000 \
-e PGID=1000 \
-e TZ=Europe/London \
-e UMASK_SET=022 \
-e WEBUI_PORT=8080 \
-p 6881:6881 \
-p 6881:6881/udp \
-p 127.0.0.1:8080:8080 \
-v /home/bogdan/Torrents/appdata:/config \
-v /home/bogdan/Torrents/Other:/downloads \
--restart unless-stopped \
linuxserver/qbittorrent 
```

Run the container:
```bash
docker start qbittorrent
```

