#Что необходимо для установки XCOM2
#Выводит следующую ошибку: error while loading shared libraries: libSDL2-2.0.so.0: cannot open shared object file: No such file or directory
#Значит, необходимо установить ээту библиотеку!
apt-get install libsdl2-2.0-0 #Или более новую версию
#Теперь такая ошибка: error while loading shared libraries: librtmp.so.0: cannot open shared object file: No such file or directory
#Но ведь она уже у нас установлена! Проверяем
locate librtmp
# И вот она, первая в списке: "/usr/lib/x86_64-linux-gnu/librtmp.so.1" но с цифрой "1", значит нужно просто переименовать (Символической ссылкой):
ln -s /usr/lib/x86_64-linux-gnu/librtmp.so.1 /usr/lib/x86_64-linux-gnu/librtmp.so.0
#Теперь всё запустится без проблем!