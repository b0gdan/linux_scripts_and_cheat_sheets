# Limiting C states on AMD
https://gist.github.com/wmealing/2dd2b543c4d3cff6cab7


To limit a CPU to a certain C-state, you can pass the `processor.max_cstate=X` option in the `kernel` line of `/boot/grub/grub.conf`.

Here we limit the system to only C-State 1:

~~~
    kernel /vmlinuz-2.6.18-371.1.2.el5 ... processor.max_cstate=1
~~~

On some systems, the kernel can override the BIOS setting, and the parameter `intel_idle.max_cstate=0` may be required to ensure sleep states are not entered:

~~~
	kernel /vmlinuz-2.6.32-431.el6.x86_64 ... processor.max_cstate=1 intel_idle.max_cstate=0
~~~

You can confirm the maximum allowed CPU C-State with:

~~~
# cat /sys/module/intel_idle/parameters/max_cstate
0
~~~

In order to save energy when the CPU is idle, the CPU can be commanded to enter a low-power mode. Each CPU has several power modes and they are collectively called “C-states” or “C-modes.”. 

The **lower-power** mode was first introduced with the 486DX4 processor. To the present, more power modes has been introduced and enhancements has been made to each mode for the CPU to consume less power in these low-power modes. The idea of these modes is to cut the clock signal and power from idle units inside the CPU. As many units you stop (by cutting the clock) as you reduce the voltage or even completely shut down to save energy. On the other hand, you have to take into account that more time is required for the CPU to “wake up” and be again 100% operational. These modes are known as **C-states**. They are usually starting in C0, which is the normal CPU operating mode, i.e., the CPU is 100% turned on. With increasing C number, the CPU sleep mode is deeper, i.e., more circuits and signals are turned off and more time the CPU will require to return to C0 mode, i.e., to wake-up. Each mode is also known by a name and several of them have sub-modes with different power saving – and thus wake-up time – levels.

mode | Name            | What id does                                                                                            | CPUs                          
-----|-----------------|---------------------------------------------------------------------------------------------------------|------------------------------
C1   | Operating State | CPU fully turned on                                                                                     | All CPUs
C1E  | Halt            | Stops CPU main internal clocks via software; bus interface unit and APIC are kept running at full speed | 486DX4 and above
C1E  | Enhanced Halt   | Stops CPU main internal clocks via software and reduces CPU voltage; bus interface unit and APIC are kept running at full speed | All socket 775 CPUs
C1E  | --              | Stops all CPU internal clocks                                                                           | Turion 64, 65-nm Athlon X2 and Phenom CPUs
C2   | Stop Grant      | Stops CPU main internal clocks via hardware; bus interface unit and APIC are kept running at full speed | 486DX4 and above
C2   | Stop Clock      | Stops CPU internal and external clocks via hardware                                                     | Only 486DX4, Pentium, Pentium MMX, K5, K6, K6-2, K6-III
C2E  | Extended Stop Grant | Stops CPU main internal clocks via hardware and reduces CPU voltage; bus interface unit and APIC are kept running at full speed | Core 2 Duo and above (Intel only)
C3   | Sleep           | Stops all CPU internal clocks                                                                           | Pentium II, Athlon and above, but not on Core 2 Duo E4000 and E6000
C3   | Deep Sleep      | Stops all CPU internal and external clocks                                                              | Pentium II and above, but not on Core 2 Duo E4000 and E6000; Turion 64
C3   | AltVID          | Stops all CPU internal clocks and reduces CPU voltage                                                   | AMD Turion 64
C4   | Deeper Sleep    | Reduces CPU voltage                                                                                     | Pentium M and above, but not on Core 2 Duo E4000 and E6000 series; AMD Turion 64
C4E/C5 | Enhanced Deeper Sleep | Reduces CPU voltage even more and turns off the memory cache                                    | Core Solo, Core Duo and 45-nm mobile Core 2 Duo only
C6   | Deep Power Down | Reduces the CPU internal voltage to any value, including 0 V                                            | 45-nm mobile Core 2 Duo only
 
## The same, more technical:
https://gist.github.com/alexandrnikitin/609dbb9d4476c5cf46c927eed5605133
```bash
# disable power saving on CentOS 7
grep -E '^model name|^cpu MHz' /proc/cpuinfo
pgrep -lf ondemand
cat /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor
for CPUFREQ in /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor; do [ -f $CPUFREQ ] || continue; echo -n performance > $CPUFREQ; done
service cpuspeed stop
lsmod | grep ondemand
grep -E '^model name|^cpu MHz' /proc/cpuinfo

# cpu freq
turbostat --debug -P
# C States
sudo cpupower monitor -m Idle_Stats

# disable intel_idle driver
grubby --default-kernel
grubby --info /boot/vmlinuz-3.10.0-514.21.2.el7.x86_64
grubby --args="intel_idle.max_cstate=0 processor.max_cstate=0 idle=poll"  --update-kernel /boot/vmlinuz-3.10.0-514.21.2.el7.x86_64
grubby --info /boot/vmlinuz-3.10.0-514.21.2.el7.x86_64
```

Checking for Hardware Errors

https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/system_administrators_guide/sec-checking_for_hardware_errors

Get current linux kernel boot parameters
```bash
cat /proc/cmdline
```
or
```bash
dmesg | grep "Command line"
```

```bash
yum install rasdaemon
```

```bash
ras-mc-ctl --summary
ras-mc-ctl --errors
```


# AMD problems from logs:
```
yum install collectd-mcelog.x86_64 mcelog.x86_64
service mcelog status


ERROR: AMD Processor family 23: mcelog does not support this processor.  Please use the edac_mce_amd module instead


echo edac_mce_amd > /etc/modules.load.d/edac_mce_amd.conf && modprobe edac_mce_amd




[    0.000000] Warning: AMD Processor - this hardware has not undergone upstream testing. Please consult http://wiki.centos.org/FAQ for more information
line 295
```